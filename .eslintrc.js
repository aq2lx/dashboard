module.exports = {
  root: true,
  env: {
    'node': true
  },
  extends: [
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:vue/essential'
  ],
  parserOptions: {
    ecmaVersion: 10,
    sourceType: 'module'
  },
  globals: {
    'gapi': 'readonly'
  },
  rules: {
    'prettier/prettier': [
      'error',
      {
        'endOfLine': 'lf',
        'semi': false,
        'singleQuote': true
      }
    ],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
