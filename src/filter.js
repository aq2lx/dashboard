import moment from 'moment'
import Vue from 'vue'

Vue.filter('plan', value => {
  const type = {
    0: 'Free',
    1: 'Silver',
    2: 'Gold',
    3: 'Premium',
    4: 'Hot Deal'
  }

  return type[value]
})

Vue.filter('date', (value, format) => {
  const d = new Date()
  const n = d.getTimezoneOffset()

  if (format === 'fromNow') {
    return moment(value)
      .add(-n, 'minutes')
      .fromNow()
  }
  return moment(value)
    .add(-n, 'minutes')
    .format(format)
})
