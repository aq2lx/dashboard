const chartConfig = {
  mapLabelLead(label) {
    const labels = {
      goToHomepage: 'Homepage',
      goToFacebook: 'Facebook',
      getLine: 'Line',
      getPhoneNumber: 'Phone',
      goToIG: 'Instagram'
    }

    return labels[label]
  }
}

export default chartConfig
