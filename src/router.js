import Vue from 'vue'
import Router from 'vue-router'

// Dashboard
const Dashboard = () => import(/* webpackChunkName: 'dashboard' */ '@/views/dashboard')

// Manage Site
const Info = () => import(/* webpackChunkName: 'info' */ '@/views/manage/info')
const Lead = () => import(/* webpackChunkName: 'lead' */ '@/views/manage/lead')
const Product = () => import(/* webpackChunkName: 'product' */ '@/views/manage/product')
// import ProductAddEdit from '@/views/manage/product/add-edit'
const Promotion = () => import(/* webpackChunkName: 'promotion' */ '@/views/manage/promotion')
// import PromotionAddEdit from '@/views/manage/promotion/add-edit'
const Gallery = () => import(/* webpackChunkName: 'gallery' */ '@/views/manage/gallery')

// Mail
const Mail = () => import(/* webpackChunkName: 'mail' */ '@/views/mail')

// Report
const Report = () => import(/* webpackChunkName: 'report' */ '@/views/report')

// Account
const Membership = () => import(/* webpackChunkName: 'membership' */ '@/views/account/membership')
const PromotionPoint = () => import(/* webpackChunkName: 'promotion-point' */ '@/views/account/promotion-point')
const MembershipHistory = () => import(/* webpackChunkName: 'membership-history' */ '@/views/account/membership-history')
const PointHistory = () => import(/* webpackChunkName: 'point-history' */ '@/views/account/point-history')
const Invoices = () => import(/* webpackChunkName: 'invoices' */ '@/views/account/invoices')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: { name: 'dashboard' }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      meta: { label: 'Dashboard' },
      component: Dashboard
    },

    // Manage
    {
      path: '/manage/info',
      name: 'manage-info',
      meta: { label: 'Manage Site / Info' },
      component: Info
    },
    {
      path: '/manage/lead',
      name: 'manage-lead',
      meta: { label: 'Manage Site / Lead' },
      component: Lead
    },

    // Product
    {
      path: '/manage/product',
      name: 'manage-product',
      meta: { label: 'Services & Products' },
      component: Product
    },
    /*{
      path: '/manage/product/:id',
      name: 'manage-product-edit',
      meta: { label: 'Services & Products / Edit' },
      component: ProductAddEdit
    },*/

    // Promotion
    {
      path: '/manage/promotion',
      name: 'manage-promotion',
      meta: { label: 'Promotion' },
      component: Promotion
    },
    /*{
      path: '/manage/promotion/:id',
      name: 'manage-promotion-edit',
      meta: { label: 'Promotions / Edit' },
      component: PromotionAddEdit
    },*/

    // Gallery
    {
      path: '/manage/gallery',
      name: 'manage-gallery',
      meta: { label: 'Gallery' },
      component: Gallery
    },

    // Mail
    {
      path: '/mail',
      name: 'mail',
      component: Mail
    },

    // Report
    {
      path: '/report',
      name: 'report',
      meta: { label: 'Report' },
      component: Report
    },

    // Account
    {
      path: '/account/membership',
      name: 'membership',
      meta: { label: 'Membership' },
      component: Membership
    },
    {
      path: '/account/promotion-point',
      name: 'promotion-point',
      meta: { label: 'Promotion Point' },
      component: PromotionPoint
    },
    {
      path: '/account/membership-history',
      name: 'membership-history',
      meta: { label: 'Membership History' },
      component: MembershipHistory
    },
    {
      path: '/account/point-history',
      name: 'point-history',
      meta: { label: 'Point History' },
      component: PointHistory
    },
    {
      path: '/account/invoices',
      name: 'invoices',
      meta: { label: 'Invoices' },
      component: Invoices
    }
  ]
})
