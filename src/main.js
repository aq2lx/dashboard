import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import './filter'

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
