// import s3 from 'aws-sdk/clients/s3'
import utils from '@/utils'

const ServiceAWS = {
  initialize() {},

  listBuckets() {},

  listAlbums() {
    this.s3.listObjects({ Delimiter: '/' }, (err, data) => {
      if (err) {
        return alert(`There was an error listing your albums: ${err.message}`)
      }
      console.log(data)
    })
  },

  createAlbum(albumName) {
    albumName = albumName.trim()

    if (!albumName) {
      return alert('Album names must contain at least one non-space character.')
    }

    if (albumName.indexOf('/') !== -1) {
      return alert('Album names cannot contain slashes.')
    }

    const albumKey = `${encodeURIComponent(albumName)}/`

    s3.headObject({ Key: albumKey }, (err, data) => {
      if (!err) {
        return alert('Album already exists.')
      }
      if (err.code !== 'NotFound') {
        return alert(`There was an error creating your album: ${err.message}`)
      }

      s3.putObject({ Key: albumKey }, (err, data) => {
        if (err) {
          return alert(`There was an error creating your album: ${err.message}`)
        }
        alert('Successfully created album.')

        console.log(data)
        // viewAlbum(albumName)
      })

      console.log(data)
    })
  },

  viewAlbum() {},

  deleteAlbum() {},

  addPhoto(albumName, files) {
    if (!files.length) {
      return alert('Please choose a file to upload first.')
    }

    const photoKey = `${encodeURIComponent(albumName)}/${utils.guid()}`

    console.log(files[0], photoKey)

    /* this.s3.upload({
      Key: photoKey,
      Body: files[0],
      ACL: 'public-read'
    }, function(err, data) {
      if (err) {
        return alert('There was an error uploading your photo: ', err.message)
      }
      alert('Successfully uploaded photo.')
      // viewAlbum(albumName);
    }).on('httpUploadProgress', function (progress) {
      this.uploadProgress = progress
      console.log(progress)
    }) */
  },

  deletePhoto() {}
}

export default ServiceAWS
