import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api'

Vue.use(Vuex)

const state = {
  sidebarOpen: false,
  vendor: {
    id: null,
    email: null,
    slug: null,
    description_en: null,
    description_th: null,
    website_url: null,
    facebook_url: null,
    line_id: null,
    profile_image_id: null,
    plan_id: null,
    points: null,
    name: null,
    profile_image: {
      id: null,
      url: './img/1x1.png'
    },
    products: [],
    promotions: [],
    videos: [],
    location: {
      name_en: null,
      name_th: null,
      additional_en: null,
      additional_th: null,
      street_en: null,
      street_th: null,
      city_en: null,
      city_th: null,
      zip_code: null
    },
    supportLanguages: []
  },
  category: [],
  subCategory: []
}

const getters = {}

const mutations = {
  closeSidebar(state) {
    state.sidebarOpen = false
  },

  toggleSidebar(state) {
    state.sidebarOpen = !state.sidebarOpen
  },

  storeVendor(state, data) {
    state.vendor = data
  }
}

const actions = {
  getVendor({ commit }, { vendorId }) {
    return new Promise((resolve, reject) => {
      api.vendor.get(vendorId).then(
        res => {
          commit('storeVendor', res.data)
          resolve()
        },
        err => {
          reject(err)
        }
      )
    })
  }
}

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

export default store
