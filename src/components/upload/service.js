// import AWS from 'aws-sdk/global'
import axios from 'axios'

const BASE_URL = 'http://localhost:3001'
const bucketName = 'aq2lx-upload'
const bucketRegion = 'ap-southeast-1'
const IdentityPoolId = 'ap-southeast-1:3dcda8bb-d222-4261-8126-9b1f3069836f'

const service = {
  initialize() {
    AWS.config.update({
      region: bucketRegion,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId
      })
    })

    this.s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      params: { Bucket: bucketName }
    })

    console.log(AWS, this.s3)

    /* region: bucketRegion,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IdentityPoolId
      }) */

    /**/

    /* this.s3 = new AWS.S3({
      params: { Bucket: bucketName }
    }) */

    // console.log(AWS, this.s3)

    // this.listAlbums()
  },

  listBuckets() {
    this.s3.listBuckets((err, data) => {
      if (err) {
        console.log('Error', err)
      } else {
        console.log('Bucket List', data.Buckets)
      }
    })
  },

  listAlbums() {
    this.s3.listObjects({ Delimiter: '/' }, (err, data) => {
      if (err) {
        return alert(`There was an error listing your albums: ${err.message}`)
      }
      console.log(data)
      /* var albums = data.CommonPrefixes.map(function(commonPrefix) {
          var prefix = commonPrefix.Prefix;
          var albumName = decodeURIComponent(prefix.replace('/', ''));
          return getHtml([
            '<li>',
              '<span onclick="deleteAlbum(\'' + albumName + '\')">X</span>',
              '<span onclick="viewAlbum(\'' + albumName + '\')">',
                albumName,
              '</span>',
            '</li>'
          ]);
        });
        var message = albums.length ?
          getHtml([
            '<p>Click on an album name to view it.</p>',
            '<p>Click on the X to delete the album.</p>'
          ]) :
          '<p>You do not have any albums. Please Create album.';
        var htmlTemplate = [
          '<h2>Albums</h2>',
          message,
          '<ul>',
            getHtml(albums),
          '</ul>',
          '<button onclick="createAlbum(prompt(\'Enter Album Name:\'))">',
            'Create New Album',
          '</button>'
        ]
        document.getElementById('app').innerHTML = getHtml(htmlTemplate); */
    })
  },

  upload(formData) {
    const url = `${BASE_URL}/photos/upload`

    return axios
      .post(url, formData)
      .then(x => x.data)
      .then(x =>
        x.map(img =>
          Object.assign({}, img, { url: `${BASE_URL}/images/${img.id}` })
        )
      )
  }
}

export default service
