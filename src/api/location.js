import axios from 'axios'
import { apiUrl } from '@/config.json'

export default {
  update(locationId, location) {
    const promise = new Promise((resolve, reject) => {
      axios
        .put(`${apiUrl}/location/${locationId}/update`, { location })
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getCountries() {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/location/countries`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getProvinces() {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/location/provinces`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  }
}
