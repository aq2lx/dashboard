import axios from 'axios'
import { apiUrl } from '@/config.json'

export default {
  get(inquiryId) {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/inquiry/${inquiryId}`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  }
}
