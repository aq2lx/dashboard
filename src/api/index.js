import googleAnalytics from './google-analytics'
import inquiry from './inquiry'
import location from './location'
import vendor from './vendor'

export default {
  googleAnalytics,
  inquiry,
  location,
  vendor
}
