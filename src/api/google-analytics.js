import axios from 'axios'
import { apiUrl } from '@/config.json'

export default {
  getToken() {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/ga-token`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  }
}
