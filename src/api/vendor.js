import axios from 'axios'
import { apiUrl } from '@/config.json'

export default {
  get(vendorId, locale = 'en') {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/vendor/${vendorId}?locale=${locale}`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  publish(vendorId, state = true) {
    const promise = new Promise((resolve, reject) => {
      axios
        .put(`${apiUrl}/vendor/${vendorId}/publish`, { state })
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  updateAboutUs(vendorId, description) {
    const promise = new Promise((resolve, reject) => {
      axios
        .put(`${apiUrl}/vendor/${vendorId}/aboutUs`, { description })
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  updateSupportLanguages(vendorId, languages) {
    const promise = new Promise((resolve, reject) => {
      axios
        .put(`${apiUrl}/vendor/${vendorId}/supportLanguages`, { languages })
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  updateLead(vendorId, data) {
    const promise = new Promise((resolve, reject) => {
      axios
        .put(`${apiUrl}/vendor/${vendorId}/lead`, { data })
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  updatePassword(vendorId, password) {
    const promise = new Promise((resolve, reject) => {
      axios
        .put(`${apiUrl}/vendor/${vendorId}/password`, { password })
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getStat(vendorId, dateFrom, dateTo) {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(
          `${apiUrl}/vendor/${vendorId}/stat?dateFrom=${dateFrom}&dateTo=${dateTo}`
        )
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getProducts(vendorId, locale = 'en') {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/vendor/${vendorId}/products?locale=${locale}`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getPromotions(vendorId, locale = 'en') {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/vendor/${vendorId}/promotions?locale=${locale}`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getGallery(vendorId) {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/vendor/${vendorId}/gallery`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  },

  getInquiries(vendorId) {
    const promise = new Promise((resolve, reject) => {
      axios
        .get(`${apiUrl}/vendor/${vendorId}/inquiries`)
        .then(res => {
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })

    return promise
  }
}
